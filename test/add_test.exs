defmodule AddTest do
  use ExUnit.Case

  test "addition" do
    assert ExcoverallsDemo.add(1, 2) == 3
  end
end
