[![Coverage Status](https://coveralls.io/repos/gitlab/ggpasqualino/excoveralls-demo/badge.svg?branch=master)](https://coveralls.io/gitlab/ggpasqualino/excoveralls-demo?branch=master)

# ExcoverallsDemo
Sample elixir project to integrate gitlab ci with coveralls.io using the parallel jobs feature.

If you have partitioned tests, you may run different partitions in each job with

`MIX_TEST_PARTITION=1 mix coveralls.gitlab --parallel --partitions 2`

`MIX_TEST_PARTITION=2 mix coveralls.gitlab --parallel --partitions 2`

Or if you have a mono repo (umbrella or poncho application), you may run diferent app tests in each job with

`mix coveralls.gitlab --parallel apps/app1/test`

`mix coveralls.gitlab --parallel apps/app2/test`

And finally, tell coveralls that all jobs are finished and summarize the result

`curl -k https://coveralls.io/webhook?repo_token=$COVERALLS_REPO_TOKEN -d "payload[build_num]=$CI_PIPELINE_ID&payload[status]=done"`
